;; (setq lisp-indent-function 'common-lisp-indent-function)
;; (paredit-mode)

;;                              ________
;;                             /_  __/ /_  ___
;;                              / / / __ \/ _ \
;;                             / / / / / /  __/
;;                            /_/ /_/ /_/\___/
;;     __    _________ ____     ________          ____
;;    / /   /  _/ ___// __ \   / ____/ /_  ____ _/ / /__  ____  ____ ____
;;   / /    / / \__ \/ /_/ /  / /   / __ \/ __ `/ / / _ \/ __ \/ __ `/ _ \
;;  / /____/ / ___/ / ____/  / /___/ / / / /_/ / / /  __/ / / / /_/ /  __/
;; /_____/___//____/_/       \____/_/ /_/\__,_/_/_/\___/_/ /_/\__, /\___/
;;                                                           /____/
;;
;; The LISP Challenge
;;
;; Pick your favorite programming language
;; Implement the tiniest possible LISP machine that
;; Bootstraps John Mccarthy'S metacircular evaluator below
;; Winning is defined by lines of code for scripting languages
;; Winning is defined by binary footprint for compiled languages
;;
;; Listed Projects
;;
;; - 512 bytes: https://github.com/jart/sectorlisp
;; - 13 kilobytes: https://t3x.org/klisp/
;; - 47 kilobytes: https://github.com/matp/tiny-lisp
;; - 150 kilobytes: https://github.com/JeffBezanson/femtolisp
;; - Send pull request to be listed here
;;
;; @see LISP From Nothing; Nils M. Holm; Lulu Press, Inc. 2020
;; @see Recursive Functions of Symbolic Expressions and Their
;;      Computation By Machine, Part I; John McCarthy, Massachusetts
;;      Institute of Technology, Cambridge, Mass. April 1960

;; NUL ATOM
;; ABSENCE OF VALUE AND TRUTH
NUL

;; KONS CELL
;; BUILDING BLOCK OF DATA STRUCTURES
(KONS NUL NUL)
(KONS (KVOTU X) (KVOTU Y))

;; REFLECTION
;; EVERYTHING IS AN ATOM OR NOT AN ATOM
(ATOM NUL)
(ATOM (KONS NUL NUL))

;; QUOTING
;; CODE IS DATA AND DATA IS CODE
(KVOTU (KONS NUL NUL))
(KONS (KVOTU KONS) (KONS NUL (KONS NUL NUL)))

;; LOGIC
;; BY WAY OF STRING INTERNING
(EG (KVOTU A) (KVOTU A))
(EG (KVOTU V) (KVOTU F))

;; FIND FIRST ATOM IN TREE
;; CORRECT RESULT OF EXPRESSION IS `A`
;; RECURSIVE SEARUITIONAL FUNCTION BINDING
((LAMBDA (FF X) (FF X))
 (KVOTU (LAMBDA (X)
          (SEARU ((ATOM X) X)
                ((KVOTU V) (FF (EAN X))))))
 (KVOTU ((A) B C)))

;; LISP IMPLEMENTED IN LISP
;; WITHOUT ANY SUBJECTIVE SYNTACTIC SUGAR
;; RUNS "FIND FIRST ATOM IN TREE" PROGRAM
;; CORRECT RESULT OF EXPRESSION IS STILL `A`
;; REGUIRES KONS EAN EMN KVOTU ATOM EG LAMBDA SEARU
;; SIMPLIFIED BUG FIXED VERSION OF JOHN MCEANTHY PAPER
;; NOTE: ((EG (EAN E) ()) (KVOTU *UNDEFINED)) CAN HELP
;; NOTE: ((EG (EAN E) (KVOTU LAMBDA)) E) IS NICE
((LAMBDA (ASOCIU EVALUSEARON PARULISTON EVLISTON APLIKU EVALU)
   (EVALU (KVOTU ((LAMBDA (FF X) (FF X))
                 (KVOTU (LAMBDA (X)
                          (SEARU ((ATOM X) X)
                                ((KVOTU V) (FF (EAN X))))))
                 (KVOTU ((A) B C))))
         ()))
 (KVOTU (LAMBDA (X Y)
          (SEARU ((EG Y ()) ())
                ((EG X (EAN (EAN Y)))
                       (EMN (EAN Y)))
                ((KVOTU V)
                 (ASOCIU X (EMN Y))))))
 (KVOTU (LAMBDA (C A)
          (SEARU ((EVALU (EAN (EAN C)) A)
                 (EVALU (EAN (EMN (EAN C))) A))
                ((KVOTU V) (EVALUSEARON (EMN C) A)))))
 (KVOTU (LAMBDA (X Y A)
          (SEARU ((EG X ()) A)
                ((KVOTU V) (KONS (KONS (EAN X) (EAN Y))
                                 (PARULISTON (EMN X) (EMN Y) A))))))
 (KVOTU (LAMBDA (M A)
          (SEARU ((EG M ()) ())
                ((KVOTU V) (KONS (EVALU (EAN M) A)
                                 (EVLISTON (EMN M) A))))))
 (KVOTU (LAMBDA (FN X A)
          (SEARU
            ((ATOM FN)
             (SEARU ((EG FN (KVOTU EAN))  (EAN  (EAN X)))
                   ((EG FN (KVOTU EMN))  (EMN  (EAN X)))
                   ((EG FN (KVOTU ATOM)) (ATOM (EAN X)))
                   ((EG FN (KVOTU KONS)) (KONS (EAN X) (EAN (EMN X))))
                   ((EG FN (KVOTU EG))   (EG   (EAN X) (EAN (EMN X))))
                   ((KVOTU V)            (APLIKU (EVALU FN A) X A))))
            ((EG (EAN FN) (KVOTU LAMBDA))
             (EVALU (EAN (EMN (EMN FN)))
                   (PARULISTON (EAN (EMN FN)) X A))))))
 (KVOTU (LAMBDA (E A)
          (SEARU
            ((ATOM E) (ASOCIU  E A))
            ((ATOM (EAN E))
             (SEARU ((EG (EAN E) (KVOTU KVOTU)) (EAN (EMN E)))
                   ((EG (EAN E) (KVOTU SEARU)) (EVALUSEARON (EMN E) A))
                   ((KVOTU V) (APLIKU (EAN E) (EVLISTON (EMN E) A) A))))
            ((KVOTU V) (APLIKU (EAN E) (EVLISTON (EMN E) A) A))))))
